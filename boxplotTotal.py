import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

def areThereAnyOutliersAbove(IQR, Values, Q3):
    hasOutliers = False
    for val in Values:
        if val > IQR*1.5+Q3:
            hasOutliers = True
    return hasOutliers

def areThereAnyOutliersBelow(IQR, Values, Q2):
    hasOutliers = False
    for val in Values:
        if val < IQR*1.5-Q2:
            hasOutliers = True
    return hasOutliers

def areThereAnyOutliers(IQR, Values, Q2, Q3):
    hasOutliers = False
    if areThereAnyOutliersAbove(IQR, Values, Q3) or areThereAnyOutliersBelow(IQR, Values, Q2):
        hasOutliers = True
    return hasOutliers
#rom util import colours
SITC1 = 'SITC-5'
SITC2 = 'SITC-6'
df = pd.read_excel('Assignment_1_Data.xlsx')
#Include only data in the df which has SITC1
df1 = df[df['SITC Code'] == SITC1]
#Get Colum for exports only
df1 = df1['Exports: Value [Tsd. EUR]'] 

df2 = df[df['SITC Code'] == SITC2]
#Get Colum for exports only
df2 = df2['Exports: Value [Tsd. EUR]']
df1List = df1.tolist()
df2List = df2.tolist()
print("Before")
print(df1List)
#Unit change
for idx, val in enumerate(df1List): 
    df1List[idx] = df1List[idx]/10**3
    df2List[idx] = df2List[idx]/10**3
print("After")
print(df1List)
both_values = [df1List, df2List]
#Plot
c = 'black'
fig, axs = plt.subplots()
axs.boxplot(both_values, patch_artist=True,
            boxprops=dict(facecolor='cyan', color='blue'),
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor='blue'),
            medianprops=dict(color=c))
axs.set_xlabel("2012-2020")
axs.set_xticklabels([SITC1, SITC2])
axs.set_title(f"Monthly total value of exported goods belonging to Group {SITC1} and {SITC2} in between 2012 to 2020")    
axs.set_ylabel('Exports: Value [Mil. EUR]')



#Calculate Outliers
#SITC1
quantmin = df1.quantile(0)
quant25 = df1.quantile(0.25)
quant50 = df1.quantile(0.5)
quant75 = df1.quantile(0.75)
quantmax = df1.quantile(1)
print(f"Das 0%-Quantil für {SITC1}: ", quantmin/10**3)
print(f"Das 25%-Quantil für {SITC1}: ", quant25/10**3)
print(f"Das 50%-Quantil für {SITC1}: ", quant50/10**3)
print(f"Das 75%-Quantil für {SITC1}: ", quant75/10**3)
print(f"Das 100%-Quantil für {SITC1}: ", quantmax/10**3)
IQR = float(quant75)-float(quant25)
hasOutliersAbove = areThereAnyOutliersAbove(IQR, df1List, float(quant75))
hasOutliersBelow = areThereAnyOutliersBelow(IQR, df1List, float(quant25))
hasOutliers = areThereAnyOutliers(IQR, df1List, float(quant25), float(quant75))
print(f"{SITC1} hasOutliers: {hasOutliers}")
#SITC2
quantmin = df2.quantile(0)
quant25 = df2.quantile(0.25)
quant50 = df2.quantile(0.5)
quant75 = df2.quantile(0.75)
quantmax = df2.quantile(1)
print(f"Das 0%-Quantil für {SITC2}: ", quantmin/10**3)
print(f"Das 25%-Quantil für {SITC2}: ", quant25/10**3)
print(f"Das 50%-Quantil für {SITC2}: ", quant50/10**3)
print(f"Das 75%-Quantil für {SITC2}: ", quant75/10**3)
print(f"Das 100%-Quantil für {SITC2}: ", quantmax/10**3)
IQR = float(quant75)-float(quant25)
hasOutliersAbove = areThereAnyOutliersAbove(IQR, df2List, float(quant75))
hasOutliersBelow = areThereAnyOutliersBelow(IQR, df2List, float(quant25))
hasOutliers = areThereAnyOutliers(IQR, df2List, float(quant25), float(quant75))
print(f"{SITC2} hasOutliers: {hasOutliers}")
#Calculate Means
print(df1.mean()/10**3)
print(df2.mean()/10**3)
#plt.show()