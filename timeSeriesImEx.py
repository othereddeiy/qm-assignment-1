#0. importing and reading in the csv in a dataframe
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.patches as mpatches
#rom util import colours
df = pd.read_excel('Assignment_1_Data.xlsx')


'''
Vorgehen:
Array nach Jahren => Import Export jeweils zusammenzählen => Plotten 
'''
#group df by column year and sum up every other column
dfByYearMean = df.groupby(["Year"]).sum()

#save number of rows
rowCount = len(dfByYearMean.index)
#Get all the years we have data for and save it in a list
YEARS = dfByYearMean.index.tolist()
#Delete last value from list because its data is not yet comparable (2021)
lastYear = YEARS.pop()
#Initialize empty list for import and export values and variables for the special last value
IM = []
EX = []
IMLastYear = 0
EXLastYear = 0
for i in range(0, 10):
    if i != 9:
        IM.append(dfByYearMean.iloc[i]['Imports: Value [Tsd. EUR]']/10**5)
        EX.append(dfByYearMean.iloc[i]['Exports: Value [Tsd. EUR]']/10**5)
    else:
        #This is necessary to plot the last year in a different colour. Perhaps there is a simpler way of doing this
        IMLastYear = dfByYearMean.iloc[i]['Imports: Value [Tsd. EUR]']/10**5
        EXLastYear = dfByYearMean.iloc[i]['Exports: Value [Tsd. EUR]']/10**5
    #print(f"Years: {YEARS}")
    #print(f"Import: {IM}")
    #print(f"Export: {EX}")
cyan_patch = mpatches.Patch(color='cyan', label="Import")
blue_patch = mpatches.Patch(color='blue', label="Export")

plt.scatter(YEARS,EX,color='black',zorder=2)
plt.scatter(YEARS,IM,color='black',zorder=2)
plt.plot(YEARS,IM,color='cyan',zorder=1)    
plt.plot(YEARS,EX,color='blue',zorder=1)
plt.scatter(lastYear, IMLastYear, color='cyan', zorder=2)
plt.scatter(lastYear, EXLastYear, color='blue', zorder=2)
plt.xlabel('Year')
plt.ylabel('Value in mil. €')  
plt.xticks(YEARS)
plt.legend(handles=[cyan_patch, blue_patch])

plt.show()
