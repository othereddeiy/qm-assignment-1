class colours:
    def __init__(self):
        self.Green = '\033[32m'
        self.White = '\033[37m'
        self.Red = '\033[31m'
        self.Yellow = '\033[33m'
        self.Blue = '\033[34m'
        self.Cyan = '\033[36m'
        self.Purple = '\033[35m'

    def greenify(self, text_to_greenify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine grüne Darstellung in der CLI ermöglichen
        '''
        return self.Green + text_to_greenify + self.White     

    def redify(self, text_to_redify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine rote Darstellung in der CLI ermöglichen
        '''
        return self.Red + text_to_redify + self.White  
    
    def purplify(self, text_to_purplify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine lila Darstellung in der CLI ermöglichen
        '''
        return self.Purple + text_to_purplify + self.White  

    def yellowfy(self, text_to_yellowfy):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine gelbe Darstellung in der CLI ermöglichen
        '''
        return self.Yellow + text_to_yellowfy + self.White  

    def cyanify(self, text_to_cyanify):
        '''
        Returned den string mit den angehängten Stringrepräsentationen die eine hellblaue Darstellung in der CLI ermöglichen
        '''
        return self.Cyan + text_to_cyanify + self.White

from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors

