import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
df = pd.read_excel('Assignment_1_Data.xlsx')

'''
Vorgehen:
>Group by Year => for every SITC-3 sum up the months => Plot every year
'''
#Get Rows where the SITC Code is SITC-3 and initialize list for that
RowsSITC3 = []
Values = dict()
dfSITC = df['SITC Code']
SITC = 'SITC-5'
#Interate through SITC Code Column
for row, val in enumerate(dfSITC):
    if val==SITC:
        #RowsSITC3.append(row)
        importVal = df['Exports: Value [Tsd. EUR]'].iloc[row]
        year = df['Year'].iloc[row]
        month = df['Month'].iloc[row]
        if year in Values:
            Values[year].append(importVal/10**3)
        else:
            Values[year] = [importVal/10**3]



#Plot  
c = 'black'
fig, ax = plt.subplots()
#fig.set_title(f"Boxplots per year of monthly {SITC}")
fig.suptitle(f"Boxplots per year of monthly {SITC}")
ax.set_ylabel('Exports: Value [Mil. €]')

ax.boxplot(Values.values(), patch_artist=True,
            boxprops=dict(facecolor='cyan', color='blue'),
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor='blue'),
            medianprops=dict(color=c))
ax.set_xticklabels(Values.keys())
print(Values.values())
plt.show()

