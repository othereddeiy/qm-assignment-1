import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('Assignment_1_Data.xlsx')

IMValues = dict()
EXValues = dict()
dfSITC = df['SITC Code']
def autopct_generator(limit):
    def inner_autopct(pct):
        return ('%.2f' % pct) if pct > limit else ''
    return inner_autopct

def getColorTuple(SITCCodes):
    clr = ('lightcyan', 'cyan', 'blue','darkcyan' , 'limegreen', 'darkgreen', 'palegreen', 'mediumspringgreen', 'azure',  'thistle')
    colors = []
    for val in SITCCodes:
        if val == 'SITC-0':
            colors.append('lightcyan')
        elif val == 'SITC-1':
            colors.append('cyan')
        elif val == 'SITC-2':
            colors.append('darkcyan')
        elif val == 'SITC-3':
            colors.append('blue')
        elif val == 'SITC-4':
            colors.append('limegreen')
        elif val == 'SITC-5':
            colors.append('darkgreen')
        elif val == 'SITC-6':
            colors.append('cyan')
        elif val == 'SITC-7':
            colors.append('palegreen')
        elif val == 'SITC-8':
            colors.append('mediumspringgreen')
        elif val == 'SITC-9':
            colors.append('cyan')
        else: 
            colors.append('yellow')
    return colors

for row, val in enumerate(dfSITC):

    #RowsSITC3.append(row)
    importVal = df['Imports: Value [Tsd. EUR]'].iloc[row]
    exportVal = df['Exports: Value [Tsd. EUR]'].iloc[row]
    year = df['Year'].iloc[row]
    month = df['Month'].iloc[row]
    if year in IMValues:
        IMValues[year].append((importVal/10**5, val))
        EXValues[year].append((exportVal/10**5, val))
    else:
        IMValues[year] = [(importVal/10**5, val)]
        EXValues[year] = [(exportVal/10**5, val)]
""" for key, val in EXValues.items():
    print(key)
    print(val) """
#Now sum up those values belonging to the same group
IMValuesSummedByGroup = {}
for val in IMValues[2019]:
    if val[1] in IMValuesSummedByGroup:
        IMValuesSummedByGroup[val[1]] += val[0]
    else:
        IMValuesSummedByGroup[val[1]] = val[0]
#print(f"IMPORT SUM: {IMValuesSummedByGroup}")
EXValuesSummedByGroup = {}
for val in EXValues[2019]:
    if val[1] in EXValuesSummedByGroup:
        EXValuesSummedByGroup[val[1]] += val[0]
    else:
        EXValuesSummedByGroup[val[1]] = val[0]
#Sort the dirctionary for a proper pie-chart but 
EXSortedList = sorted(EXValuesSummedByGroup.items(), key=lambda x: x[1], reverse=True)
IMSortedList = sorted(IMValuesSummedByGroup.items(), key=lambda x: x[1], reverse=True)
#Restore the dict
EXSorted = {}
for val in EXSortedList:
    EXSorted[val[0]] = val[1]
IMSorted = {}
for val in IMSortedList:
    IMSorted[val[0]] = val[1]
#print(f"EXPORT SUM: {EXValuesSummedByGroup}")
print(IMSorted)
print(EXSorted)
#print(IMValues[2019])
#colors = ('lightcyan', 'cyan', 'blue','darkcyan' , 'limegreen', 'darkgreen', 'palegreen', 'mediumspringgreen', 'azure',  'thistle' )
fig, axs = plt.subplots(1, 2)
fig.suptitle('Value of Import and Export Goods categorized by group in 2019')
axs[0].pie(EXSorted.values(),labels=EXSorted.keys(), autopct=autopct_generator(5),
            shadow=True, startangle=90, colors=getColorTuple(EXSorted.keys())
)
axs[0].set_title('Export') 
axs[1].pie(IMSorted.values(),labels=IMSorted.keys(), autopct=autopct_generator(5),
            shadow=True, startangle=90, colors=getColorTuple(IMSorted.keys(),)
)
axs[1].set_title('Import')        
        
plt.show()