import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
df = pd.read_excel('Assignment_1_Data.xlsx')

'''
Vorgehen:
>Group by Year => for every SITC-3 sum up the months => Plot every year
'''
#Get Rows where the SITC Code is SITC-3 and initialize list for that
RowsSITC3 = []
Values = dict()
dfSITC = df['SITC Code']
#Interate through SITC Code Column
for row, val in enumerate(dfSITC):
    if val=="SITC-5":
        #RowsSITC3.append(row)
        importVal = df['Imports: Value [Tsd. EUR]'].iloc[row]
        year = df['Year'].iloc[row]
        month = df['Month'].iloc[row]
        if year in Values:
            Values[year].append(importVal/10**5)
        else:
            Values[year] = [importVal/10**5]



#Plot  
c = 'black'
fig, axs = plt.subplots(5, 2, figsize=(5,3))
counter = 2012

for i in range(0,5):
    for j in range(0,2):
        axs[i, j].boxplot(Values[counter], patch_artist=True,
            boxprops=dict(facecolor='cyan', color='blue'),
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor='blue'),
            medianprops=dict(color=c)
            )
        print(Values[counter])
        axs[i,j].set_title(str(counter))
        counter +=1

plt.show()

""" fig, (ax) = plt.subplots(nrows=1, ncols=9, figsize=(9, 4))
labels = [str(i) for i in range(2012, 2021)]
print(labels)
counter = 2012

ax.boxplot(Values[counter], patch_artist=True, labels=labels)
print(Values[counter])
#axs[i,j].set_title(str(counter))
#fig[i,j].set_title(str(counter))
counter +=1
plt.show() """
