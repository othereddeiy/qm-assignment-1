#0. importing and reading in the excel into the dataframe variable
import pandas as pd
import matplotlib.pyplot as plt
from util import colours 
import matplotlib.patches as mpatches
df = pd.read_excel('Assignment_1_Data.xlsx')
clr = colours()
def getColours(values):
    colours_list = []
    lastval = 0
    for idx, val in enumerate(values):
        if lastval < val and idx != 0:
            colours_list.append('springgreen')
        elif idx == 0:
            colours_list.append('cyan')
        else: 
            colours_list.append('lightcoral')
        lastval = val
    return colours_list

'''
Vorgehen:
Array nach Jahren => Import Export zusammenzählen => Subplots 
'''
#group df by column year and sum up every other column
dfByYearMean = df.groupby(["Year"]).sum()
#Fürs Aussehen des Plots
labels = ("Export", "Import")
colors = ('cyan', 'blue')
#save number of rows
rowCount = len(dfByYearMean.index)
#Set number of subplots
fig, axs = plt.subplots()
YEARS = dfByYearMean.index
counter = -1
lastRatio = 0
RATIOS = []

for i in range(0, 5):
    for j in range(0, 2):
        counter += 1
        IM = dfByYearMean.iloc[counter]['Imports: Value [Tsd. EUR]']
        EX = dfByYearMean.iloc[counter]['Exports: Value [Tsd. EUR]']
        
        ratio = IM/EX
        if ratio > lastRatio:
            print(clr.greenify(str(ratio)))
        else:
            print(clr.redify(str(ratio)))
        print(f"CHANGE: {lastRatio-ratio}")  
        lastRatio = ratio
        RATIOS.append(ratio)
        #print(lastRatio)
""" 
        
        sizes = [EX, IM]
        print(f"Import: {IM}")
        print(f"Export: {EX}")
        fig.suptitle('German Trade Volume partitioned between Import and Export in between 2012 and July 2021')
        axs[i, j].pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90, colors=colors)
        if i==4 and j==1:
            axs[i, j].set_title("2021 till July")
        else: 
            axs[i, j].set_title(YEARS[counter])      """ 
green_patch = mpatches.Patch(color='springgreen', label="increased")
blue_patch = mpatches.Patch(color='cyan', label="N/A")
red_patch = mpatches.Patch(color='lightcoral', label="decreased")

print(YEARS)
YEARSList = YEARS.tolist()
YEARSList.pop()
RATIOS.pop()
colours_list = getColours(RATIOS)
plt.bar(YEARSList, RATIOS, color=colours_list)
plt.xticks(YEARSList)
plt.ylabel("Import/Export Ratio")
plt.xlabel("Years")
plt.legend(handles=[green_patch, blue_patch, red_patch])
plt.show()
