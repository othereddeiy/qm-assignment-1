import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_excel('Assignment_1_Data.xlsx')

df = df.groupby('Year').mean()
'''
used for 1a) => Calcualte the mean rate of change of the value of exported and imported goods
'''
print(df['Imports: Value [Tsd. EUR]'].pct_change())
print(df['Exports: Value [Tsd. EUR]'].pct_change())

print(df['Imports: Value [Tsd. EUR]'].pct_change().mean())
print(df['Exports: Value [Tsd. EUR]'].pct_change().mean())