import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

#from determineOutliers import SITC


df = pd.read_excel('Assignment_1_Data.xlsx')

#Dropping Values from 2021
#Dropping Values from 2020
df = df[df.Year == 2020]
df = df[df['SITC Code'] == 'SITC-6']
df = df.groupby('Month').mean()
#print(dfMonthlyMeans)
Exports = df['Exports: Value [Tsd. EUR]']
ExportVal = []
rowVal = []
print(Exports)
c = 'black'
fig, axs = plt.subplots()
plt.scatter(df.index, Exports/10**3, color='black',zorder=2)
plt.plot(Exports/10**3, color = 'cyan', zorder=1)
plt.xlabel("Months")
plt.title(f"Export values of manufactured goods (SITC-6) by month in 2020")    
plt.ylabel('Exports: Value [Mil. EUR]')
plt.xticks(df.index)
plt.show()