import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
#rom util import colours
df = pd.read_excel('Assignment_1_Data.xlsx')

'''
Vorgehen:
Group by Year => for every SITC-3 sum up the months => Plot every year
'''
#Get Rows where the SITC Code is SITC-3 and initialize list for that
RowsSITC3 = []
Values = dict()
dfSITC = df['SITC Code']
for row, val in enumerate(dfSITC):
    if val=="SITC-5":
        #RowsSITC3.append(row)
        importVal = df['Imports: Value [Tsd. EUR]'].iloc[row]
        year = df['Year'].iloc[row]
        month = df['Month'].iloc[row]
        if year in Values:
            Values[year].append(importVal/10**3)
        else:
            Values[year] = [importVal/10**3]


#Plot here 
fig, axs = plt.subplots()
#axs.set_title('Box Plot')
yearToShow = 2019
axs.boxplot(Values[yearToShow])
print(Values[yearToShow])
axs.set_title(f"SITC-5 in {str(yearToShow)}")
#fig[i,j].set_title(str(counter))

plt.show()