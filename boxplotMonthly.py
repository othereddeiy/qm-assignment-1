import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
#from determineOutliers import hasOutliers
df = pd.read_excel('Assignment_1_Data.xlsx')

'''
Vorgehen:
>Get df for SITC => for every SITC-3 sum up the months => Plot every year
'''
#Get Rows where the SITC Code is SITC-3 and initialize list for that
RowsSITC3 = []
Values = dict()
dfSITC = df['SITC Code']
SITC = 'SITC-5'
#Interate through SITC Code Column
for row, val in enumerate(dfSITC):
    if val==SITC:
        #RowsSITC3.append(row)
        importVal = df['Imports: Value [Tsd. EUR]'].iloc[row]
        year = df['Year'].iloc[row]
        month = df['Month'].iloc[row]
        if month in Values:
            Values[month].append(importVal/10**5)
        else:
            Values[month] = [importVal/10**5]

print(Values.values())

#Plot  
c = 'black'
fig, ax = plt.subplots()
#fig.set_title(f"Boxplots per year of monthly {SITC}")
fig.suptitle(f"Boxplots per month of group {SITC}")
ax.set_ylabel('Imports: Value [Mil. €]')

ax.boxplot(Values.values(), patch_artist=True,
            boxprops=dict(facecolor='cyan', color='blue'),
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor='blue'),
            medianprops=dict(color=c))
ax.set_xticklabels(Values.keys())
print(Values.values())
plt.show()
