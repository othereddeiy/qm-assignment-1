import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

#from determineOutliers import SITC


df = pd.read_excel('Assignment_1_Data.xlsx')


#Dropping Values from 2021
#Dropping Values from 2020
df = df[df.Year != 2021]
df = df[df.Year != 2020]
df = df[df['SITC Code'] == 'SITC-6']
print(df.head(100))
dfMonthlyMeans = df.groupby('Month').mean()
#print(dfMonthlyMeans)
Exports = dfMonthlyMeans['Exports: Value [Tsd. EUR]']
Exports = Exports.div(10**3).round(2)
ExportVal = []
rowVal = []
""" for row, val in enumerate(dfMonthlyMeans):
    ExportVal.append(val)
    rowVal.append(row)
print(ExportVal) """
print(Exports)
c = 'black'
fig, axs = plt.subplots()
plt.scatter(Exports.index, Exports, color='black',zorder=2)
plt.plot(Exports, color = 'cyan', zorder=1)
plt.xlabel("Months")
plt.title(f"Monthly averages for the export values of manufactured goods (SITC-6) from 2012 to 2019")    
plt.ylabel('Exports: Value [Tsd. EUR]')
plt.xticks(dfMonthlyMeans.index)
plt.show()