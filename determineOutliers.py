import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

def areThereAnyOutliersAbove(IQR, Values, Q3):
    hasOutliers = False
    for val in Values:
        if val > IQR*1.5+Q3:
            hasOutliers = True
    return hasOutliers

def areThereAnyOutliersBelow(IQR, Values, Q2):
    hasOutliers = False
    for val in Values:
        if val < IQR*1.5-Q2:
            hasOutliers = True
    return hasOutliers

def areThereAnyOutliers(IQR, Values, Q2, Q3):
    hasOutliers = False
    if areThereAnyOutliersAbove(IQR, Values, Q3) or areThereAnyOutliersBelow(IQR, Values, Q2):
        hasOutliers = True
    return hasOutliers

df = pd.read_excel('Assignment_1_Data.xlsx')

'''
Vorgehen:
>Group by Year => for every SITC-3 sum up the months => Plot every year
'''
#Get Rows where the SITC Code is SITC-3 and initialize list for that
RowsSITC3 = []
Values = dict()
dfSITC = df['SITC Code']
SITC = 'SITC-6'
#Interate through SITC Code Column
for row, val in enumerate(dfSITC):
    if val==SITC:
        #RowsSITC3.append(row)
        importVal = df['Imports: Value [Tsd. EUR]'].iloc[row]
        year = df['Year'].iloc[row]
        month = df['Month'].iloc[row]
        if year in Values:
            Values[year].append(importVal/10**5)
        else:
            Values[year] = [importVal/10**5]
#Calculate the IQR per year
QuartileValues = dict()
MeanValues = dict()

for key, val in Values.items():
    #Add the Quartiles to the Quartile-dictionary
    year = key
    # for i in val:
    #     if year in QuartileValues:
    #         QuartileValues[year].append(i/10**5)
    #     else:
    #         QuartileValues[year] = [i/10**5]
    
    dfValues = pd.DataFrame (val, columns = ['values'])
    quantmin = dfValues.quantile(0)
    quant25 = dfValues.quantile(0.25)
    quant50 = dfValues.quantile(0.5)
    quant75 = dfValues.quantile(0.75)
    quantmax = dfValues.quantile(1)
    mean = dfValues.mean()
    '''
    print(f"Das 0%-Quantil im Jahr {year}: ", quantmin)
    print(f"Das 25%-Quantil im Jahr {year}: ", quant25)
    print(f"Das 50%-Quantil im Jahr {year}: ", quant50)
    print(f"Das 75%-Quantil im Jahr {year}: ", quant75)
    print(f"Das 100%-Quantil im Jahr {year}: ", quantmax)
    '''
    IQR = float(quant75)-float(quant25)
    hasOutliersAbove = areThereAnyOutliersAbove(IQR, val, float(quant75))
    hasOutliersBelow = areThereAnyOutliersBelow(IQR, val, float(quant25))
    hasOutliers = areThereAnyOutliers(IQR, val, float(quant25), float(quant75))
    '''
    print(f"{year} has outliers below: {hasOutliersBelow}")
    print(f"{year} has outliers above: {hasOutliersAbove}")
    print(f"{year} has outliers: {hasOutliers}")
    '''
    if year in QuartileValues:
        QuartileValues[year].append([float(quantmin), float(quant25), float(quant50), float(quant75), float(quantmax), hasOutliers])
    else:
        QuartileValues[year] = [float(quantmin), float(quant25), float(quant50), float(quant75), float(quantmax), hasOutliers]

    MeanValues[year] = float(mean)
    #QuartileValues[year] 
    #print(f" TYPE : {type(float(quantmin))}")
    #print(QuartileValues)
for key, val in QuartileValues.items():
    print(val)
for key, val in MeanValues.items():
    print(f"Mittelwert im Jahr {key} = {val}")
MeanValuesExcl2021 = MeanValues
MeanValuesExcl2021.pop(2021)

#Plot
c = 'black'
fig, axs = plt.subplots()
axs.boxplot(MeanValues.values(), patch_artist=True,
            boxprops=dict(facecolor='cyan', color='blue'),
            capprops=dict(color=c),
            whiskerprops=dict(color=c),
            flierprops=dict(color=c, markeredgecolor='blue'),
            medianprops=dict(color=c))
axs.set_xlabel("2012-2020")
axs.set_title(f"Means of Value of imported goods belonging to Group {SITC} in between 2012 to 2020")    
axs.set_ylabel('Imports: Value [Mil. EUR]')
plt.show()

