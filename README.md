# QM-Assignment-1 
This repo contains some files which individually are used to plot data for assignment 1

## Which files are used for which figure in the documentation:
Figure 1: `timeSeriesImEx.py`
Figure 2: `pieChartByGroups.py`
Figure 3: `boxPlotTotal.py`
Figure 4: `boxPlotTotal.py`
Figure 5: `boxPlotTotal.py`
Figure 6: `boxPlotTotal.py` 
Figure 7: `timeSeriesMonthlyAverages.py`
Figure 8: `timeSeriesOnly2020.py`
Figure 9: `pieChartExIm.py`
Figure 10: `meanRateOfChange.py`
Figure 11: `areThereAnyOutliers()` in `timeSeriesOnly2020.py`

The rest of the files also show different non-requested graphs for the task on hand ✌️